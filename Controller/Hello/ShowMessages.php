<?php

namespace Assaka\Helloworld\Controller\Hello;
use Magento\Framework\App\Action\Context;

class ShowMessages extends \Magento\Framework\App\Action\Action
{

    public function __construct(Context $context
    ) {
        parent::__construct($context);
    }

    public function execute()
    {

        echo 'displaying message';
        $this->messageManager->addSuccess('Message from new controller.');

    }

}