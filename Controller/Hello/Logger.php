<?php

namespace Assaka\Helloworld\Controller\Hello;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Message\ManagerInterface;

class Logger extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;
    private $messageManager;

    public function __construct(\Psr\Log\LoggerInterface $logger, Context $context
//        Data $dataHelper
    ) {
        parent::__construct($context);

        $this->logger = $logger;

//        $this->_dataHelper = $dataHelper;
    }

    public function execute()
    {

        echo 'logging helloworld';

        $this->logger->info('Logging from helloworld/logger');
//        return $this;
    }
}
