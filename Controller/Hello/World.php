<?php
namespace Assaka\Helloworld\Controller\Hello;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Message\ManagerInterface;

class World extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;
    protected $_pageFactory;
    protected $_dataHelper;
    protected $_messageManager;
    protected $_logger;

    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Psr\Log\LoggerInterface $logger
//        Data $dataHelper
    ) {
        parent::__construct($context);
        $this->_pageFactory = $pageFactory;
        $this->_messageManager = $messageManager;
        $this->logger = $logger;
//        $this->_dataHelper = $dataHelper;
    }

    public function execute()
    {

        $pageFactory = $this->_pageFactory->create();

        // Add title which is got by the configuration via backend
//        $pageFactory->getConfig()->getTitle()->set(
//            $this->_dataHelper->getHeadTitle()
//        );

        // Add breadcrumb
        /** @var \Magento\Theme\Block\Html\Breadcrumbs */
        $breadcrumbs = $pageFactory->getLayout()->getBlock('breadcrumbs');
        $breadcrumbs->addCrumb('home',
            [
                'label' => __('Home'),
                'title' => __('Home'),
                'link' => $this->_url->getUrl('')
            ]
        );
        $breadcrumbs->addCrumb('helloworld',
            [
                'label' => __('Hello world'),
                'title' => __('Hello world')
            ]
        );

        $this->_eventManager->dispatch(
            'update_helloworld_today'
        );

        $this->logger->info('Logging from helloworld/world');
        $this->messageManager->addSuccess('Message from new controller.');
        $this->_messageManager->addError('not my world');

        $this->_view->loadLayout();
        $this->_view->renderLayout();



    }
}