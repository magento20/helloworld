<?php
/**
 * Created by PhpStorm.
 * User: Gebruiker
 * Date: 5/3/2016
 * Time: 1:52 PM
 */

namespace Assaka\Helloworld\Cron;


class UpdateBlog
{
    protected $_logger;

    public function __construct(\Psr\Log\LoggerInterface $logger) {
        $this->_logger = $logger;
    }

    public function execute()
    {
        $this->_logger->info(__METHOD__);
        return $this;
    }

}